# Rollcaller Script for Python

---

### 使用

1.你需要在你的系统中安装Python(=>3.7)运行时

2.克隆这个仓库

```
git clone https://gitlab.com/lishengxlin/rollcaller_public.git
```

3.进入目录

```
cd rollcaller_public
```

4.修改listfile.txt

你需要下列格式的listfile.txt:

```
张三[空格]
李四[空格]
王五[空格]
李华[空格]
```

末行不要为空

5.运行

对于*nix(例如macOS,Linux.....):

```
python3 rollcaller_unix.py
```

对于Windows:

第一种方法:双击打开

第二种:使用Windows控制台

> 需要Python在你的PATH中

> 若出现乱码，请在执行脚本前输入:<kbd>chcp 65001</kbd>

```
python rollcaller_win32.py
```

### 更新脚本

```
cd rollcaller_public
git fetch
```
